const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Products = require("../models/Products");


module.exports.getUsers = (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  console.log(userData);

  if (userData.isAdmin === true) {
    return User.find().then((result) => {
      result.password = "***";
      return res.send(result);
    });
  } else {
    res.send(false);
  }
};

module.exports.makeAdmin = (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin === true) {
    return User.findByIdAndUpdate(
      req.body.userId,
      {
        isAdmin: req.body.isAdmin,
      },
      {
        new: true,
      }
    )
      .then((result) => {
        res.send(true);
      })
      .catch((error) => {
        console.log(error);
        res.send(false);
      });
  } else {
    return res.status(401).send(false);
  }
};

module.exports.addToCart = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  let isUserUpdated = User.findById(userData.id).then((user) => {
    const itemExist = user.cart.filter((item) => {
      return item.productId === req.body.productId;
    });

    if (itemExist.length == 0) {
      user.cart.push({
        productId: req.body.productId,
        productName: req.body.productName,
        quantity: req.body.quantity,
        price: req.body.price,
        imgSrc: req.body.imgSrc,
      });
    } else {
      itemExist[0].quantity += req.body.quantity;
    }

    return user
      .save()
      .then((result) => res.send(result))
      .catch((error) => {
        console.log(error);
        return false;
      });
  });
};

// User registration

module.exports.registerUser = (req, res) => {
  let newUser = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    // Syntax: bcrypt.hashSync(dataToBeEncrypted, salt);
    // salt - salt rounds that the bcyrpt alogorithm will run to encrypt the password.
    password: bcrypt.hashSync(req.body.password, 10),
    mobileNumber: req.body.mobileNumber,
  });

  return newUser
    .save()
    .then((user) => {
      console.log(user);
      // send "true" if the user is created
      res.send(true);
    })
    .catch((error) => {
      console.log(error);
      // send "false" if any error is encountered.
      res.send(false);
    });
};

// Check if the email exists

module.exports.checkEmailExists = async (req, res) => {
  return await User.find({ email: req.body.email })
    .then((result) => {
      // The result of the find() method returns an array of objects.
      // we can use array.length method for checking the current result length

      // The user already exists
      if (result.length > 0) {
        return res.send(true);
        // return res.send("User already exists!");
      }
      // There are no duplicate found.
      else {
        return res.send(false);
        // return res.send("No duplicate found!");
      }
    })
    .catch((error) => res.send(error));
};

// User authentication (login)

module.exports.loginUser = (req, res) => {
  return User.findOne({ email: req.body.email }).then((result) => {
    if (result == null) {
      return res.send(true);
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        req.body.password,
        result.password
      );

      // If the passwords match/result of the above code is true.
      if (isPasswordCorrect) {
        // Generate an access token
        // Uses the "createAccessToken" method defined in the "auth.js" file
        // Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects
        return res.send({ accessToken: auth.createAccessToken(result) });
      } else {
        return res.send(false); // if password do not match
        // return res.send({message: "Incorrect password!"});
      }
    }
  });
};

module.exports.userDetails = (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  return User.findById(userData.id).then((result) => {
    result.password = "***";
    res.send(result);
  });
};

module.exports.addToCart = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  let isUserUpdated = User.findById(userData.id).then((user) => {
    const itemExist = user.cart.filter((item) => {
      return item.productId === req.body.productId;
    });

    if (itemExist.length == 0) {
      user.cart.push({
        productId: req.body.productId,
        productName: req.body.productName,
        quantity: req.body.quantity,
        price: req.body.price,
        imgSrc: req.body.imgSrc,
      });
    } else {
      itemExist[0].quantity += req.body.quantity;
    }

    return user
      .save()
      .then((result) => res.send(result))
      .catch((error) => {
        console.log(error);
        return false;
      });
  });
};

module.exports.removeToCart = (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  let isUserUpdated = User.findById(userData.id).then((user) => {
    user.cart = user.cart.filter((item) => {
      return item.productId !== req.body.productId;
    });

    return user
      .save()
      .then((result) => res.send(result))
      .catch((error) => {
        console.log(error);
        return false;
      });
  });
};

module.exports.checkOut = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  const cartItems = req.body.cart;

  if (userData.isAdmin !== true) {
    let isUserUpdated = await User.findById(userData.id).then((user) => {
      user.orders.push({
        totalAmount: req.body.totalAmount,
        products: cartItems,
      });

      user.cart.splice(0, user.cart.length);

      return user.save().catch((error) => {
        console.log(error);
        return false;
      });
    });

    let isProductUpdated = cartItems.forEach((item) => {
      Products.findByIdAndUpdate(item.productId).then((product) => {
        product.stocks -= item.quantity;

        product.orders.push({
          userId: userData.id,
          userEmail: userData.email,
          quantity: item.quantity,
        });

        return product.save().catch((error) => {
          console.log(error);
          return false;
        });
      });
    });
  } else {
    res.send(false);
  }
};
