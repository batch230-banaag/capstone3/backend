const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

console.log(userControllers);

// Route for user registration
router.get("/", auth.verify, userControllers.getUsers);

// Route for user registration
router.post("/register", userControllers.registerUser);

// Route for user authentication
router.post("/login", userControllers.loginUser);

// Routes for checking email
router.post("/checkEmail", userControllers.checkEmailExists);

// Route for user details
router.get("/userDetails", auth.verify, userControllers.userDetails);

router.post("/addCart", auth.verify, userControllers.addToCart);

router.post("/removeCart", auth.verify, userControllers.removeToCart);

// 
router.post("/checkOut", auth.verify, userControllers.checkOut);

// Route for making user admin
router.patch("/makeAdmin", auth.verify, userControllers.makeAdmin);

module.exports = router;
